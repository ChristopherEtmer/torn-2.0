﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class YtoZ : MonoBehaviour {

	Vector2 playerBase;
	public BoxCollider2D myCollider;

	void Awake()
	{

	}

	// Update is called once per frame
	void LateUpdate () 
	{
		playerBase = myCollider.bounds.center;
		this.transform.position =new Vector3(this.transform.position.x,this.transform.position.y,(playerBase.y/100));
	}

	void OnDrawGizmos()
	{
		Gizmos.DrawWireSphere (playerBase, 0.1f);
	}
}
