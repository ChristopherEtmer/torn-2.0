﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Manager : MonoBehaviour {

	public static Manager instance;
	public GameObject dialogueText;
	public GameObject PlayerImage;
	public GameObject NpcImage;
	GameObject continueText;
	Text text;
	public string currentSentence;

	public GameObject inventory;

	// Use this for initialization
	void Start () {
		//making this a Singleton
		if (instance != null) 
		{
			Destroy (this.gameObject);
		}
		else
		{
			instance = this;
		}
		DontDestroyOnLoad (this.gameObject);

		// Setting up dialogue Properties
		dialogueText = GameObject.FindGameObjectWithTag("dialoguePanel");
		PlayerImage = GameObject.FindGameObjectWithTag ("PlayerImage");
		NpcImage = GameObject.FindGameObjectWithTag ("NPCImage");
		text = GameObject.FindGameObjectWithTag ("PanelText").GetComponent<Text>();
		continueText = GameObject.Find ("continueText").gameObject;
		PlayerImage.SetActive (false);
		NpcImage.SetActive (false);
		dialogueText.SetActive (false);

		//Setting up inventory
		inventory = GameObject.FindGameObjectWithTag("inventory");

	}

	public void displayText(string sentence,string currentSpeaker)
	{
		dialogueText.SetActive (true);	
		print ("displayText");
		continueText.SetActive (false);
		string content = null;
		if (currentSpeaker == "Player")
		{
			PlayerImage.SetActive(true);
			NpcImage.SetActive(false);
			content += "Player: ";
		}
		else
		{
			PlayerImage.SetActive(false);
			NpcImage.SetActive(true);
			content += "NPC: ";
		}
		currentSentence = content + sentence;
		StartCoroutine (charDelay (content+sentence));
	}

	public void endDialogue()
	{
		text.text = null;
		dialogueText.SetActive (false);
		PlayerImage.SetActive (false);
		NpcImage.SetActive (false);
	}

	IEnumerator charDelay(string content)
	{
		text.text = null;

		foreach (char n in content) {

			text.text += n;
			yield return null;
		}
		continueText.SetActive (true);
		currentSentence = "";
		StopCoroutine(charDelay(null));
	}

}
