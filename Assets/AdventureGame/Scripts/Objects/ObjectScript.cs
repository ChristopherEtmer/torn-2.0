﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ObjectScript : MonoBehaviour, IObject 
{
	public enum Speaker
	{
	Player,
	Npc
	}
	public Speaker[] _speaker;
	public dialogueLines sentence;
	public Queue<Speaker> speakingPerson;
	public Queue<string> _dialogue;

	public void startInteraction ()
	{
		Manager.instance.dialogueText.SetActive (true);
		speakingPerson = new Queue<Speaker> (_speaker.Length);
		_dialogue = new Queue<string>(sentence.line.Length);
		foreach (string n in sentence.line) 
		{
			_dialogue.Enqueue (n);
		}
		foreach (Speaker n in _speaker)
		{
			speakingPerson.Enqueue (n);
		}
		response ();
	}

	public bool response()
	{
		if (_dialogue.Count == 0) 
		{
			Manager.instance.endDialogue ();
			Debug.Log ("Möp");
			return false;
		}
		if (Manager.instance.currentSentence.Length == 0) {
			Speaker currentSpeaker = speakingPerson.Dequeue (); 
			string sentence = _dialogue.Dequeue ();
			Manager.instance.displayText (sentence, currentSpeaker.ToString ());
		}
		else 
		{
			Debug.Log ("Sentence in Queue!");
		}
		return true;
	
	}

}



