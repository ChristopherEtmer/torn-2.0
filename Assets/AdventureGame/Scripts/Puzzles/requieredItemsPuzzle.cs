﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class requieredItemsPuzzle : MonoBehaviour, IObject {

	public inventoryController inventory;
	public GameObject requieredItems;
	List <GameObject> requieredItemsList;
	[TextArea]
	public string[] description;
	int dialogueIndex;

	void Start()
	{
		requieredItemsList = new List<GameObject> ();
		inventory = FindObjectOfType<inventoryController> ();
		for (int i = 0; i < requieredItems.transform.childCount; i++)
		{
			requieredItemsList.Add(requieredItems.transform.GetChild(i).gameObject);
		}

	}
	public void  startInteraction()
	{
		string currentLine = content (); 
		if (dialogueIndex != 0) {
			foreach (GameObject n in requieredItemsList) {
				if (inventory.checkForItem (n.name)) {
					n.SetActive (true);
				} else {
					currentLine += '\n';
					currentLine += n.name + " ";
				}
			}
		}
			
		Manager.instance.displayText (currentLine, "Player");
	}

	string content()
	{
		if (dialogueIndex == 0) {
			string currentContent = description [dialogueIndex];
			dialogueIndex++;
			return currentContent;
		}
		else 
		{
			if (!checkItemState ()) {
				return description [1];
			}
			else 
			{
				return description [2];
			}
		}
		return description [dialogueIndex];
	}

	bool checkItemState()
	{
		for (int i = 0; i < requieredItems.transform.childCount; i++)
		{
			if (requieredItemsList [i].activeSelf == false) 
			{
				return false;
			}
		}
		return true;
	}
	public bool response()
	{
		Manager.instance.endDialogue ();
		return false;
	}
}
