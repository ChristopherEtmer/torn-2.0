﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class inventoryController : MonoBehaviour {

	public inventoyData data;
	public List<string> Names;
	public Image imagePrefab;

	void Start ()
	{

		data = new inventoyData ();

	}
	
	public bool checkForItem(string itemName)
	{
		if (data.itemNames.Contains (itemName)) 
		{
			return true;
		} 
		else 
		{
			return false;
		}
	}

	public bool addItem(string itemName)
	{
		if (!checkForItem (itemName)) 
		{
			addItemImage ();
			data.itemNames.Add (itemName);
			Names.Add (itemName);
			return true;
		} 
		else 
		{
			Debug.Log ("can not add " + itemName + " since it is already in the inventory");
			return false;
		}
	}
	void addItemImage()
	{
		
		Image Slot = Instantiate (imagePrefab);
		Slot.transform.parent = Manager.instance.inventory.transform;
		Vector3 startPosition = Vector3.zero + (Vector3.left * ((Camera.main.pixelWidth / 2)-100));
		Slot.rectTransform.localPosition = startPosition + Vector3.right * (data.itemNames.Count * (imagePrefab.rectTransform.rect.width+10));

	}
}
