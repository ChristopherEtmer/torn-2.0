﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraConstrains : MonoBehaviour {

	GameObject Player;
	[Tooltip("Drag the GameObject that contains the Background Sprite in here")]
	public GameObject backGround;
    Bounds bounds;
	float valueX;
	float valueY;
	float sizeX;
	float sizeY;
	public Vector2 constrainsX;
	public Vector2 constrainsY;
	float aspect;
	// Use this for initialization

	void Awake() {
		aspect =Camera.main.aspect;
		sizeX = aspect * Camera.main.orthographicSize;
		sizeY = (Camera.main.orthographicSize/aspect)*aspect;
		//finding Player on start
		Player = GameObject.FindGameObjectWithTag ("Player");
		bounds = backGround.GetComponent<SpriteRenderer> ().sprite.bounds;
		constrainsX.x = (bounds.min.x * backGround.transform.localScale.x) + backGround.transform.localPosition.x;
		constrainsX.y = (bounds.max.x * backGround.transform.localScale.x) + backGround.transform.localPosition.x;
		constrainsY.x = (bounds.min.y * backGround.transform.localScale.y) + backGround.transform.localPosition.y;
		constrainsY.y = (bounds.max.y * backGround.transform.localScale.y) + backGround.transform.localPosition.y;
	}
	
	// Update is called once per frame
	void LateUpdate () {
		valueX = Player.transform.position.x;
		valueY = Player.transform.position.y;

		clampValues ();

		this.transform.position = new Vector3 (valueX, valueY,  -10);
		
	}

	void clampValues()
	{
		

		valueX = Mathf.Clamp (valueX, constrainsX.x+sizeX, constrainsX.y-sizeX);

		valueY = Mathf.Clamp (valueY, constrainsY.x+sizeY, constrainsY.y-sizeY);
	}

	}
