﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemScript : MonoBehaviour, IObject {

	inventoryController controller;

	public dialogueLines sentence;
	public int index;

	void Start()
	{
		controller = FindObjectOfType<inventoryController> ();
	}
	public void startInteraction ()
	{
		Manager.instance.dialogueText.SetActive (true);	
		response ();
	}

	public bool response()
	{
		if (index == sentence.line.Length) {
			index = 0;
			if(controller.addItem (this.gameObject.name)){
			Debug.LogWarning (this.name + " added to inventory");
			Manager.instance.endDialogue ();
			Destroy (this.gameObject);
			}
			return false;
		}

		Manager.instance.displayText (sentence.line [index], "Player");
		index++;
		return true;
	}
}
