﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class UIManager : MonoBehaviour {

	public GameObject npcImage;
	public GameObject playerImage;
	public GameObject textBox;

	void Start()
	{
		
	}

	void openTextBox(string content)
	{
		textBox.SetActive (true);
	
	}
	void closeTextBox()
	{
		textBox.SetActive (false);
		npcImage.SetActive (false);
		playerImage.SetActive (false);
	}
}
